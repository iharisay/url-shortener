'use strict'

process.env.AWS_XRAY_CONTEXT_MISSING = 'LOG_ERROR';
process.env.NODE_ENV = 'dev';

const merge = require('deepmerge')
const assert = require('assert');
const index = require('../src/index');

let axios = require('axios');
let MockAdapter = require('axios-mock-adapter');
let axiosMock = new MockAdapter(axios);

let eventMaster = {
    "path": "/url-shortener/v1",
    "queryStringParameters": {"longUrl":"https://example.com"},
    "requestContext": {
        "accountId": "offlineContext_accountId",
        "resourceId": "offlineContext_resourceId",
        "apiId": "offlineContext_apiId",
        "stage": "dev",
        "requestId": "offlineContext_requestId_32612684918639556",
        "identity": {
            "cognitoIdentityPoolId": "offlineContext_cognitoIdentityPoolId",
            "accountId": "offlineContext_accountId",
            "cognitoIdentityId": "offlineContext_cognitoIdentityId",
            "caller": "offlineContext_caller",
            "apiKey": "offlineContext_apiKey",
            "sourceIp": "127.0.0.1",
            "cognitoAuthenticationType": "offlineContext_cognitoAuthenticationType",
            "cognitoAuthenticationProvider": "offlineContext_cognitoAuthenticationProvider",
            "userArn": "offlineContext_userArn",
            "userAgent": "PostmanRuntime/6.4.1",
            "user": "offlineContext_user"
        },
        "authorizer": {
            "principalId": "offlineContext_authorizer_principalId"
        },
        "httpMethod": "GET"
    },
    "httpMethod": "GET",
    "queryStringParameters": null,
    "stageVariables": null,
    "isOffline": true
};

describe('Handler Tests', function() {
    it('PUT should not be supported', async () => {

        let event = merge(eventMaster, {
            "path": "/url-shortener/v1",
            "queryStringParameters": {"longUrl":"https://example.com"},
            "httpMethod": "PUT",
            "requestContext": {
                "httpMethod": "PUT"
            },
            "body": JSON.stringify({
                "id": "test@test.com",
                "lastName": "Jacobs"
            })
        })

        try {
            let response = await index.handler(event);
            assert.deepEqual(response,{
                "statusCode": 405,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Methods": "*",
                    "Access-Control-Allow-Credentials": "false",
                    "Access-Control-Allow-Headers": "*",
                    "Access-Control-Allow-Origin": "*"
                },
                "body": "{\"status\":405,\"messages\":\"httpMethod: PUT is not supported\"}"
            }, 'expected response');
        } catch (err) {
            assert.fail(err);
        } finally {}
    })

    it('GET success', async () => {
        axiosMock.onGet(/.*/).reply(200, {
            "long_url": "https://example.com/",
            "hash": "vxssZh",
            "short_url": "http://nskn.co/vxssZh",
            "new_hash": 0,
            "alt_hashes": [
              
            ],
            "status_txt": "OK",
            "status_code": 0
          });

        let event = merge(eventMaster, {
            "path": "/url-shortener/v1",
            "queryStringParameters": {"longUrl":"https://example.com"},
            "httpMethod": "GET",
            "requestContext": {
                "httpMethod": "GET"
            }
        })

        try {
            let response = await index.handler(event);
            assert.deepEqual(response,{
                "statusCode": 200,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Methods": "*",
                    "Access-Control-Allow-Credentials": "false",
                    "Access-Control-Allow-Headers": "*",
                    "Access-Control-Allow-Origin": "*"
                },
                "body": JSON.stringify({
                    "long_url": "https://example.com/",
                    "hash": "vxssZh",
                    "short_url": "http://nskn.co/vxssZh",
                    "new_hash": 0,
                    "alt_hashes": [
                      
                    ],
                    "status_txt": "OK",
                    "status_code": 0
                  })
            }, 'expected response');
        } catch (err) {
            assert.fail(err);
        } finally {}
    })

    it('GET pass a bad apiKey', async () => {
        axiosMock.onGet(/.*/).reply(400, {
            "status_txt": "AUTH_ERROR",
            "status_code": 41
          });

        let event = merge(eventMaster, {
            "path": "/url-shortener/v1",
            "queryStringParameters": {"longUrl":"https://example.com", "apiKey": "fakeApiKey"},
            "httpMethod": "GET",
            "requestContext": {
                "httpMethod": "GET"
            }
        })

        try {
            let response = await index.handler(event);
            assert.deepEqual(response,{
                "statusCode": 400,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Methods": "*",
                    "Access-Control-Allow-Credentials": "false",
                    "Access-Control-Allow-Headers": "*",
                    "Access-Control-Allow-Origin": "*"
                },
                "body": JSON.stringify({
                    "status_txt": "AUTH_ERROR",
                    "status_code": 41
                  })
            }, 'expected response');
        } catch (err) {
            assert.fail(err);
        } finally {}
    })
});