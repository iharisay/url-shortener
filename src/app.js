'use strict';

const awsXRay = require('aws-xray-sdk-core');
const AWS = awsXRay.captureAWS(require('aws-sdk'));
const REGION = process.env.AWS_REGION || 'us-west-2';
const nsAwsUtils = require('ns-aws-utils');
const https = require('https')
const axios = require('axios')
const log = nsAwsUtils.logger;
AWS.config.update({region: REGION});

const VENDOR_ENDPOINT = process.env.VENDOR_ENDPOINT
const API_KEY = process.env.API_KEY
const TIMEOUT = 60000;

/**
 * The get function facilitates all get requests to make to Annex Cloud
 * 
 * @param {Object} event - 
 * @throws {Object} errorObject 
 * @returns {Object} response 
 */
const get = async (event) => {
    let response;
    try {
        let longUrl = decodeURIComponent(event.queryStringParameters.longUrl)
        let apiKey = event.queryStringParameters.apiKey ? event.queryStringParameters.apiKey : API_KEY


        // build the request
        let config = {
            method: 'GET',
            url: VENDOR_ENDPOINT,
            params: {
                longUrl: longUrl,
                apiKey: apiKey
            },
            headers: {
                'Content-Type': 'application/json',
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false }),
            timeout: TIMEOUT
        };
        
        log.info({axiosGetConfig: config});

        response = await axios(config);
    } catch(error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            log.info({errorResponse: error.response});
            response = error.response
            // throw formatError(error.response.status, JSON.stringify(error.response.data))
        } else {
            // Something happened in setting up the request that triggered an Error
            log.info({error: error.message});
            throw formatError(error.status || 400, error.message)
        }
    }
    return formatAxiosResponse(response);
}

const formatAxiosResponse = (response) => {
    return {
        statusCode: response.status,
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(response.data)
    };
}

/**
 * Builds an error response object.
 *
 * @param {string} status
 * @param {string} message
 * @return {Object}
 */
// function formatError(status, message) {
//     return { status: status, message: message }
// }

module.exports = {
    get
};