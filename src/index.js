'use strict';

const app = require('./app');
const nsAwsUtils = require('ns-aws-utils');
const cors = nsAwsUtils.cors;
const log = nsAwsUtils.logger;
const eidify = nsAwsUtils.eidify;
log.setLevel(process.env.LOG_LEVEL);

/**
 * handler is a Lambda function.  The AWS Lambda service will invoke this function when a given event and runtime.
 * According to the AWS ASAP training, you need to invoke the 'callback'.  If not, the Lambda will wait for five
 * minutes then finish.   That means we need to pay for the full five minutes processing time.
 *
 * @param {Object} event an event data is passed by AWS Lambda service
 */
async function handler(event) {
    log.setTag(); // clear previous tags
    log.info({event: event});
    let response = await cors(eidify(processHTTP))(event);
    log.info({response: response});
    return response;

}

/**
 * Processes HTTP request to GET, POST, PUT, and DELETE paymentProfile records.
 *
 * @param event
 * @return {Promise.<void>}
 */
async function processHTTP(event) {
    let response = {};

    try {
        switch (event.httpMethod) {
            case 'GET':
                response = await app.get(event);
                break;
            default:
                throw {status: 405, message: `httpMethod: ${event.httpMethod} is not supported`};
        }
    } catch (error) {
        log.error(error);
        response = formatErrorResponse(error.status || 400,  error.message);
    }

    return response;
}


/**
 * Builds an error response object.
 *
 * @param {string} status
 * @param {string} messages
 * @return {Object}
 */
function formatErrorResponse(status, messages) {
    return {
        statusCode: status,
        body: JSON.stringify({
            status: status,
            messages: messages
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    };
}


/**
 * xrayHandler is a workaround function for solving a problem with xray not compatible with node8.
 * https://github.com/aws/aws-xray-sdk-node/issues/27 (Rich has found out this problem)
 *
 * @param {object} event an event data is passed by AWS Lambda service
 * @param {object} context a runtime information is passed by AWS Lambda service
 * @param {function} callback a callback function
 */
// function xrayHandler(event, context, callback) {
//     handler(event, context)
//         .then((res) => callback(null, res))
//         .catch((err) => callback(err));
// }

module.exports = {
    handler
};